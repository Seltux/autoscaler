package config

import (
	"testing"

	"github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConfigMarshal(t *testing.T) {
	data := `
MaximumTimeout = 1800
ExecutionMaxRetries = 4
Port = 1234
`
	expectedCfg := Executor{
		MaximumTimeout:      1800,
		ExecutionMaxRetries: 4,
		Port:                0,
	}

	var cfg Executor
	err := toml.Unmarshal([]byte(data), &cfg)
	require.NoError(t, err)

	assert.Equal(t, expectedCfg, cfg)
}
