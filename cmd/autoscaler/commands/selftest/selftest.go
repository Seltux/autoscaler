package selftest

import (
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
)

func NewSelftTestCategory() cli.Category {
	return cli.Category{
		Config: cli.Config{
			Name:   "selftest",
			Usage:  "Commands useful for binary testing",
			Hidden: true,
		},
		SubCommands: []cli.Command{
			NewLogCheckCommand(),
			NewSignalCheckCommand(),
		},
	}
}
