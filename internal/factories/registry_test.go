package factories

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type noop struct{}

var (
	testFactoryType = "test"
	testFactoryName = "test"
	testFactory     = new(noop)
)

func TestErrNotRegistered_Error(t *testing.T) {
	err := NewErrNotRegistered("type", "name")
	assert.EqualError(t, err, `type "name" is not registered`)
}

func TestErrAlreadyRegistered_Error(t *testing.T) {
	err := NewErrAlreadyRegistered("type", "name")
	assert.EqualError(t, err, `type "name" is already registered`)
}

func TestRegistry_MustRegister(t *testing.T) {
	tests := map[string]struct {
		duplicateRegistration bool
	}{
		"registered once": {
			duplicateRegistration: false,
		},
		"duplicate registration": {
			duplicateRegistration: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			r := NewRegistry(testFactoryType)
			assert.NotPanics(t, func() {
				r.MustRegister(testFactoryName, testFactory)
			})

			if testCase.duplicateRegistration {
				assert.Panics(t, func() {
					r.MustRegister(testFactoryName, testFactory)
				})
			}
		})
	}
}

func TestRegistry_Get(t *testing.T) {
	tests := map[string]struct {
		factoryToFind string
		expectedError error
	}{
		"registered factory": {
			factoryToFind: testFactoryName,
			expectedError: nil,
		},
		"unregistered factory": {
			factoryToFind: "unknown",
			expectedError: NewErrNotRegistered(testFactoryType, "unknown"),
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			r := NewRegistry(testFactoryType)
			require.NotPanics(t, func() {
				r.MustRegister(testFactoryName, testFactory)
			})

			f, err := r.Get(testCase.factoryToFind)
			if testCase.expectedError != nil {
				assert.Equal(t, testCase.expectedError, err)

				return
			}

			assert.NoError(t, err)
			assert.Equal(t, testFactory, f)
		})
	}
}

func TestRegistry_IsRegistered(t *testing.T) {
	tests := map[string]struct {
		factoryToFind  string
		expectedResult bool
	}{
		"registered factory": {
			factoryToFind:  testFactoryName,
			expectedResult: true,
		},
		"unregistered factory": {
			factoryToFind:  "unknown",
			expectedResult: false,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			r := NewRegistry(testFactoryType)
			require.NotPanics(t, func() {
				r.MustRegister(testFactoryName, testFactory)
			})

			result := r.IsRegistered(testCase.factoryToFind)
			assert.Equal(t, testCase.expectedResult, result)
		})
	}
}
