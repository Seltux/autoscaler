package orka

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPurgeVM(t *testing.T) {
	type purgeVMTestCase struct {
		testCaseBase

		name string
	}

	testCases := map[string]purgeVMTestCase{
		"purge vm is successful": {
			testCaseBase: testCaseBase{
				token:      "token1",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Message: "Successfully purged VM",
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			name: testVMName,
		},
		"with name of non-existent VM": {
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusBadRequest,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Errors: respBodyErrors{
							{
								Message: "No VMs with that Name exist in Orka",
							},
							{
								Message: "Some other error",
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedDeployRequestBody("non-existent"),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointPurge),
					StatusCode: http.StatusBadRequest,
					Message:    "",
					Errors:     []string{"No VMs with that Name exist in Orka", "Some other error"},
				},
			},
			name: "non-existent",
		},
		"purge vm is successful after retry on StatusServiceUnavailable": {
			testCaseBase: testCaseBase{
				token:      "token1",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Message: "Successfully purged VM",
					},
				},
				backoffSettings:         testBackoff,
				expectedRequestAttempts: 2,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			name: testVMName,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, http.MethodDelete, &tc.testCaseBase,
				func(c *client) error {
					return c.PurgeVM(context.Background(), tc.name)
				})
		})
	}
}

func getExpectedPurgeResponseBody(vmName string) purgeVMReqBody {
	return purgeVMReqBody{VMName: vmName}
}

func TestPurgeVMReqBodyJSONSerialization(t *testing.T) {
	testCases := []purgeVMReqBody{
		{
			VMName: "myorkavm1",
		},
		{
			VMName: "myorkavm2",
		},
	}

	for _, tc := range testCases {
		json := serializeToJSON(t, tc)
		expectedJSON := fmt.Sprintf(`{"orka_vm_name":"%s"}`, tc.VMName)

		assert.Equal(t, json, expectedJSON)
	}
}
